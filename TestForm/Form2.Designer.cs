﻿namespace TestForm
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbimie = new System.Windows.Forms.Label();
            this.lbprawo = new System.Windows.Forms.Label();
            this.lbplec = new System.Windows.Forms.Label();
            this.lbstatus = new System.Windows.Forms.Label();
            this.lbnazwisko = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbimie
            // 
            this.lbimie.AutoSize = true;
            this.lbimie.Location = new System.Drawing.Point(132, 13);
            this.lbimie.Name = "lbimie";
            this.lbimie.Size = new System.Drawing.Size(21, 13);
            this.lbimie.TabIndex = 0;
            this.lbimie.Text = "lb1";
            // 
            // lbprawo
            // 
            this.lbprawo.AutoSize = true;
            this.lbprawo.Location = new System.Drawing.Point(132, 214);
            this.lbprawo.Name = "lbprawo";
            this.lbprawo.Size = new System.Drawing.Size(35, 13);
            this.lbprawo.TabIndex = 1;
            this.lbprawo.Text = "label2";
            // 
            // lbplec
            // 
            this.lbplec.AutoSize = true;
            this.lbplec.Location = new System.Drawing.Point(132, 168);
            this.lbplec.Name = "lbplec";
            this.lbplec.Size = new System.Drawing.Size(35, 13);
            this.lbplec.TabIndex = 2;
            this.lbplec.Text = "label3";
            // 
            // lbstatus
            // 
            this.lbstatus.AutoSize = true;
            this.lbstatus.Location = new System.Drawing.Point(132, 114);
            this.lbstatus.Name = "lbstatus";
            this.lbstatus.Size = new System.Drawing.Size(35, 13);
            this.lbstatus.TabIndex = 3;
            this.lbstatus.Text = "label4";
            // 
            // lbnazwisko
            // 
            this.lbnazwisko.AutoSize = true;
            this.lbnazwisko.Location = new System.Drawing.Point(132, 59);
            this.lbnazwisko.Name = "lbnazwisko";
            this.lbnazwisko.Size = new System.Drawing.Size(35, 13);
            this.lbnazwisko.TabIndex = 4;
            this.lbnazwisko.Text = "label5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Prawo jazdy:";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbnazwisko);
            this.Controls.Add(this.lbstatus);
            this.Controls.Add(this.lbplec);
            this.Controls.Add(this.lbprawo);
            this.Controls.Add(this.lbimie);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbimie;
        private System.Windows.Forms.Label lbprawo;
        private System.Windows.Forms.Label lbplec;
        private System.Windows.Forms.Label lbstatus;
        private System.Windows.Forms.Label lbnazwisko;
        private System.Windows.Forms.Label label1;
    }
}
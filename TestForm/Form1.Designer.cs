﻿namespace TestForm
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.TEST = new System.Windows.Forms.Label();
            this.Plec = new System.Windows.Forms.Label();
            this.Stanowisko = new System.Windows.Forms.Label();
            this.Nazwisko = new System.Windows.Forms.Label();
            this.Imie = new System.Windows.Forms.Label();
            this.Prawo = new System.Windows.Forms.Label();
            this.tbxImie = new System.Windows.Forms.TextBox();
            this.tbxNazwiskocc = new System.Windows.Forms.TextBox();
            this.lista = new System.Windows.Forms.ComboBox();
            this.radioMen = new System.Windows.Forms.RadioButton();
            this.radioInna = new System.Windows.Forms.RadioButton();
            this.cbPrawo = new System.Windows.Forms.CheckBox();
            this.Przycisk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TEST
            // 
            this.TEST.AutoSize = true;
            this.TEST.Location = new System.Drawing.Point(60, 26);
            this.TEST.Name = "TEST";
            this.TEST.Size = new System.Drawing.Size(52, 13);
            this.TEST.TabIndex = 0;
            this.TEST.Text = "Formularz";
            // 
            // Plec
            // 
            this.Plec.AutoSize = true;
            this.Plec.Location = new System.Drawing.Point(60, 219);
            this.Plec.Name = "Plec";
            this.Plec.Size = new System.Drawing.Size(30, 13);
            this.Plec.TabIndex = 1;
            this.Plec.Text = "Płeć";
            // 
            // Stanowisko
            // 
            this.Stanowisko.AutoSize = true;
            this.Stanowisko.Location = new System.Drawing.Point(60, 187);
            this.Stanowisko.Name = "Stanowisko";
            this.Stanowisko.Size = new System.Drawing.Size(62, 13);
            this.Stanowisko.TabIndex = 2;
            this.Stanowisko.Text = "Stanowisko";
            // 
            // Nazwisko
            // 
            this.Nazwisko.AutoSize = true;
            this.Nazwisko.Location = new System.Drawing.Point(60, 140);
            this.Nazwisko.Name = "Nazwisko";
            this.Nazwisko.Size = new System.Drawing.Size(53, 13);
            this.Nazwisko.TabIndex = 3;
            this.Nazwisko.Text = "Nazwisko";
            // 
            // Imie
            // 
            this.Imie.AutoSize = true;
            this.Imie.Location = new System.Drawing.Point(60, 77);
            this.Imie.Name = "Imie";
            this.Imie.Size = new System.Drawing.Size(26, 13);
            this.Imie.TabIndex = 4;
            this.Imie.Text = "Imie";
            // 
            // Prawo
            // 
            this.Prawo.AutoSize = true;
            this.Prawo.Location = new System.Drawing.Point(60, 258);
            this.Prawo.Name = "Prawo";
            this.Prawo.Size = new System.Drawing.Size(77, 13);
            this.Prawo.TabIndex = 5;
            this.Prawo.Text = "Prawo jazdy B.";
            // 
            // tbxImie
            // 
            this.tbxImie.Location = new System.Drawing.Point(144, 74);
            this.tbxImie.Name = "tbxImie";
            this.tbxImie.Size = new System.Drawing.Size(100, 20);
            this.tbxImie.TabIndex = 6;
            // 
            // tbxNazwiskocc
            // 
            this.tbxNazwiskocc.Location = new System.Drawing.Point(144, 140);
            this.tbxNazwiskocc.Name = "tbxNazwiskocc";
            this.tbxNazwiskocc.Size = new System.Drawing.Size(100, 20);
            this.tbxNazwiskocc.TabIndex = 7;
            // 
            // lista
            // 
            this.lista.FormattingEnabled = true;
            this.lista.Items.AddRange(new object[] {
            "malarz",
            "murarz",
            "tynkarz",
            "zbrojarz"});
            this.lista.Location = new System.Drawing.Point(128, 187);
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(121, 21);
            this.lista.TabIndex = 8;
            // 
            // radioMen
            // 
            this.radioMen.AutoSize = true;
            this.radioMen.Location = new System.Drawing.Point(128, 219);
            this.radioMen.Name = "radioMen";
            this.radioMen.Size = new System.Drawing.Size(49, 17);
            this.radioMen.TabIndex = 9;
            this.radioMen.TabStop = true;
            this.radioMen.Text = "MEN";
            this.radioMen.UseVisualStyleBackColor = true;
            // 
            // radioInna
            // 
            this.radioInna.AutoSize = true;
            this.radioInna.Location = new System.Drawing.Point(219, 219);
            this.radioInna.Name = "radioInna";
            this.radioInna.Size = new System.Drawing.Size(46, 17);
            this.radioInna.TabIndex = 10;
            this.radioInna.TabStop = true;
            this.radioInna.Text = "Inna";
            this.radioInna.UseVisualStyleBackColor = true;
            // 
            // cbPrawo
            // 
            this.cbPrawo.AutoSize = true;
            this.cbPrawo.Location = new System.Drawing.Point(154, 257);
            this.cbPrawo.Name = "cbPrawo";
            this.cbPrawo.Size = new System.Drawing.Size(15, 14);
            this.cbPrawo.TabIndex = 11;
            this.cbPrawo.UseVisualStyleBackColor = true;
            // 
            // Przycisk
            // 
            this.Przycisk.Location = new System.Drawing.Point(189, 270);
            this.Przycisk.Name = "Przycisk";
            this.Przycisk.Size = new System.Drawing.Size(122, 23);
            this.Przycisk.TabIndex = 12;
            this.Przycisk.Text = "Podsumowanie";
            this.Przycisk.UseVisualStyleBackColor = true;
            this.Przycisk.Click += new System.EventHandler(this.Przycisk_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Przycisk);
            this.Controls.Add(this.cbPrawo);
            this.Controls.Add(this.radioInna);
            this.Controls.Add(this.radioMen);
            this.Controls.Add(this.lista);
            this.Controls.Add(this.tbxNazwiskocc);
            this.Controls.Add(this.tbxImie);
            this.Controls.Add(this.Prawo);
            this.Controls.Add(this.Imie);
            this.Controls.Add(this.Nazwisko);
            this.Controls.Add(this.Stanowisko);
            this.Controls.Add(this.Plec);
            this.Controls.Add(this.TEST);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TEST;
        private System.Windows.Forms.Label Plec;
        private System.Windows.Forms.Label Stanowisko;
        private System.Windows.Forms.Label Nazwisko;
        private System.Windows.Forms.Label Imie;
        private System.Windows.Forms.Label Prawo;
        private System.Windows.Forms.TextBox tbxImie;
        private System.Windows.Forms.TextBox tbxNazwiskocc;
        private System.Windows.Forms.ComboBox lista;
        private System.Windows.Forms.RadioButton radioMen;
        private System.Windows.Forms.RadioButton radioInna;
        private System.Windows.Forms.CheckBox cbPrawo;
        private System.Windows.Forms.Button Przycisk;
    }
}

